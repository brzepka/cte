<?php
App::uses('AppController', 'Controller');
/**
 * MatieresUsers Controller
 *
 * @property MatieresUser $MatieresUser
 * @property PaginatorComponent $Paginator
 */
class MatieresUsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->MatieresUser->recursive = 0;
		$this->set('matieresUsers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MatieresUser->exists($id)) {
			throw new NotFoundException(__('Invalid matieres user'));
		}
		$options = array('conditions' => array('MatieresUser.' . $this->MatieresUser->primaryKey => $id));
		$this->set('matieresUser', $this->MatieresUser->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MatieresUser->create();
			if ($this->MatieresUser->save($this->request->data)) {
				$this->Session->setFlash(__('The matieres user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The matieres user could not be saved. Please, try again.'));
			}
		}
		$matieres = $this->MatieresUser->Matiere->find('list');
		$users = $this->MatieresUser->User->find('list');
		$this->set(compact('matieres', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MatieresUser->exists($id)) {
			throw new NotFoundException(__('Invalid matieres user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MatieresUser->save($this->request->data)) {
				$this->Session->setFlash(__('The matieres user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The matieres user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('MatieresUser.' . $this->MatieresUser->primaryKey => $id));
			$this->request->data = $this->MatieresUser->find('first', $options);
		}
		$matieres = $this->MatieresUser->Matiere->find('list');
		$users = $this->MatieresUser->User->find('list');
		$this->set(compact('matieres', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MatieresUser->id = $id;
		if (!$this->MatieresUser->exists()) {
			throw new NotFoundException(__('Invalid matieres user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MatieresUser->delete()) {
			$this->Session->setFlash(__('The matieres user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The matieres user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
