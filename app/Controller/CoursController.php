<?php
App::uses('AppController', 'Controller');
/**
 * Cours Controller
 *
 * @property Cour $Cour
 * @property PaginatorComponent $Paginator
 */
class CoursController extends AppController {

/**
 * Components
 *
 * @var array
 */
    //var $helpers = array('AjaxMultiUpload.Upload');
    public $helpers = array('Html', 'Form');
    public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Cour->recursive = 0;
		$this->set('cours', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cour->exists($id)) {
			throw new NotFoundException(__('Invalid cour'));
		}
		$options = array('conditions' => array('Cour.' . $this->Cour->primaryKey => $id));
		$this->set('cour', $this->Cour->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')){
			$heure_debut = array();
			$heure_fin = array();
			$date = array();

			list($heure_debut["hour"], $heure_debut["min"]) = split(":",$this->request->data["Cour"]["heure_debut"]);
			list($heure_fin["hour"], $heure_fin["min"]) = split(":",$this->request->data["Cour"]["heure_fin"]);
			list($date["year"], $date["month"], $date["day"]) = split("/",$this->request->data["Cour"]["date"]);

			$this->request->data["Cour"]["heure_debut"] = $heure_debut;
			$this->request->data["Cour"]["heure_fin"] = $heure_fin;
			$this->request->data["Cour"]["date"] = $date;
			debug($this->request->data);
			// die;
			$this->Cour->create();
			if ($this->Cour->save($this->request->data)) {
				$this->Session->setFlash(__('The cour has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cour could not be saved. Please, try again.'));
			}
		}
		$matieres = $this->Cour->Matiere->find('list');
        $promotions = $this->Cour->Promotion->find('list');
		$this->set(compact('matieres', 'promotions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cour->exists($id)) {
			throw new NotFoundException(__('Invalid cour'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cour->save($this->request->data)) {
				$this->Session->setFlash(__('The cour has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cour could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Cour.' . $this->Cour->primaryKey => $id));
			$this->request->data = $this->Cour->find('first', $options);
		}
		$matieres = $this->Cour->Matiere->find('list');
        $promotions = $this->Cour->Promotion->find('list');
		$this->set(compact('matieres','promotions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cour->id = $id;
		if (!$this->Cour->exists()) {
			throw new NotFoundException(__('Invalid cour'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Cour->delete()) {
			$this->Session->setFlash(__('The cour has been deleted.'));
		} else {
			$this->Session->setFlash(__('The cour could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
