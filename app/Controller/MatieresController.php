<?php
App::uses('AppController', 'Controller');
/**
 * Matieres Controller
 *
 * @property Matiere $Matiere
 * @property PaginatorComponent $Paginator
 */
class MatieresController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Matiere->recursive = 0;
		$this->set('matieres', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Matiere->exists($id)) {
			throw new NotFoundException(__('Invalid matiere'));
		}
		$options = array('conditions' => array('Matiere.' . $this->Matiere->primaryKey => $id));
		$this->set('matiere', $this->Matiere->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Matiere->create();
			if ($this->Matiere->save($this->request->data)) {
				$this->Session->setFlash(__('The matiere has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The matiere could not be saved. Please, try again.'));
			}
		}
		$users = $this->Matiere->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Matiere->exists($id)) {
			throw new NotFoundException(__('Invalid matiere'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Matiere->save($this->request->data)) {
				$this->Session->setFlash(__('The matiere has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The matiere could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Matiere.' . $this->Matiere->primaryKey => $id));
			$this->request->data = $this->Matiere->find('first', $options);
		}
		$users = $this->Matiere->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Matiere->id = $id;
		if (!$this->Matiere->exists()) {
			throw new NotFoundException(__('Invalid matiere'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Matiere->delete()) {
			$this->Session->setFlash(__('The matiere has been deleted.'));
		} else {
			$this->Session->setFlash(__('The matiere could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
