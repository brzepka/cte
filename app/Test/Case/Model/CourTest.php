<?php
App::uses('Cour', 'Model');

/**
 * Cour Test Case
 *
 */
class CourTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.cour',
		'app.matieres'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Cour = ClassRegistry::init('Cour');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Cour);

		parent::tearDown();
	}

}
