<?php
App::uses('Planning', 'Model');

/**
 * Planning Test Case
 *
 */
class PlanningTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.planning',
		'app.cour',
		'app.matieres',
		'app.plannings_cour'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Planning = ClassRegistry::init('Planning');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Planning);

		parent::tearDown();
	}

}
