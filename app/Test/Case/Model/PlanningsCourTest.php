<?php
App::uses('PlanningsCour', 'Model');

/**
 * PlanningsCour Test Case
 *
 */
class PlanningsCourTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.plannings_cour',
		'app.planning',
		'app.cour',
		'app.matieres',
		'app.cours'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PlanningsCour = ClassRegistry::init('PlanningsCour');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PlanningsCour);

		parent::tearDown();
	}

}
