<?php
App::uses('Matiere', 'Model');

/**
 * Matiere Test Case
 *
 */
class MatiereTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.matiere',
		'app.user',
		'app.matieres_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Matiere = ClassRegistry::init('Matiere');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Matiere);

		parent::tearDown();
	}

}
