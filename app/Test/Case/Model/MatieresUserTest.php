<?php
App::uses('MatieresUser', 'Model');

/**
 * MatieresUser Test Case
 *
 */
class MatieresUserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.matieres_user',
		'app.matiere',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MatieresUser = ClassRegistry::init('MatieresUser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MatieresUser);

		parent::tearDown();
	}

}
