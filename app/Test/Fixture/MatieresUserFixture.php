<?php
/**
 * MatieresUserFixture
 *
 */
class MatieresUserFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'matiere_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'indexes' => array(
			'PRIMARY' => array('column' => array('matiere_id', 'user_id'), 'unique' => 1),
			'fk_matieres_has_users_users1_idx' => array('column' => 'user_id', 'unique' => 0),
			'fk_matieres_has_users_matieres_idx' => array('column' => 'matiere_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'matiere_id' => 1,
			'user_id' => 1
		),
	);

}
