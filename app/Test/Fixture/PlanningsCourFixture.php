<?php
/**
 * PlanningsCourFixture
 *
 */
class PlanningsCourFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'planning_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'cours_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'indexes' => array(
			'PRIMARY' => array('column' => array('planning_id', 'cours_id'), 'unique' => 1),
			'fk_planning_has_cours_cours1_idx' => array('column' => 'cours_id', 'unique' => 0),
			'fk_planning_has_cours_planning1_idx' => array('column' => 'planning_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'planning_id' => 1,
			'cours_id' => 1
		),
	);

}
