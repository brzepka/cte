<?php
App::uses('AppModel', 'Model');
/**
 * Cour Model
 *
 * @property Matieres $Matieres
 */
class Cour extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Matiere' => array(
			'className' => 'Matiere',
			'foreignKey' => 'matieres_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
        'Promotion'=> array(
            'className' => 'Promotion',
            'foreignKey' => 'promotions_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
