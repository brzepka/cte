<?php
/**
 * Created by PhpStorm.
 * User: baptisterzepka
 * Date: 04/05/2014
 * Time: 04:41
 */?>

<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <?php
            echo $this->Html->meta('icon');

            echo $this->Html->css('cake.generic');
            echo $this->Html->css('foundation');
            echo $this->Html->css('normalize');
            echo $this->Html->css('default');

            echo $this->Html->script('foundation/foundation');

            echo $this->fetch('meta');
            echo $this->fetch('css');
            echo $this->fetch('script');
        ?>
    </head>
    <body>
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->fetch('content'); ?>
    </body>
</html>
