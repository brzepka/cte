<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */


?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>

    </title>
    <?php
		echo $this->Html->meta('icon');

    //echo $this->Html->css('cake.generic');
    echo $this->Html->css('bootstrap');
    echo $this->Html->css('bootstrap-theme');
    echo $this->Html->css('default');

    echo $this->Html->script('jquery');
    echo $this->Html->script('bootstrap.min');

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid" >
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Cahier de Texte Electronique</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> Admin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><?php echo $this->Html->link(__('Mon Compte'), array('controller' => 'users', 'action' => 'view', )); ?></li>
                        <li><?php echo $this->Html->link(__('Se deconnecter'), array('controller' => 'users', 'action' => 'logout', )); ?></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-right" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Recherche">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>
                </button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <div class="menu">
                <span class="glyphicon glyphicon-th"></span><?php echo $this->Html->link(__(' Dashboard'), array('controller' => 'users', 'action' => 'dashboard')); ?><br>
                <span class="glyphicon glyphicon-calendar"></span><?php echo $this->Html->link(__(' Cours'), array('controller' => 'plannings', 'action' => 'index', )); ?><br>
                <span class="glyphicon glyphicon-briefcase"></span><?php echo $this->Html->link(__(' Devoirs'), array('controller' => 'cours', 'action' => 'index', )); ?><br>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="menu">
                <span class="glyphicon glyphicon-th"></span><?php echo $this->Html->link(__(' Dashboard'), array('controller' => 'users', 'action' => 'dashboard')); ?><br>
                <span class="glyphicon glyphicon-calendar"></span><?php echo $this->Html->link(__(' Cours'), array('controller' => 'plannings', 'action' => 'index', )); ?><br>
                <span class="glyphicon glyphicon-briefcase"></span><?php echo $this->Html->link(__(' Devoirs'), array('controller' => 'cours', 'action' => 'index', )); ?><br>
            </div>
        </div>
        <div class="col-lg-10 ">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>
        </div>
    </div>
</div>
</body>

</html>
