<div class="mywell">
	<div class="cours index">
		<h2><?php echo __('Cours'); ?></h2>
		<table cellpadding="0" cellspacing="0" class="table table-bordered">
		<tr class="info">
				<th><?php echo $this->Paginator->sort('author', "Auteur"); ?></th>
				<th><?php echo $this->Paginator->sort('name', "Intitulé"); ?></th>
				<th><?php echo $this->Paginator->sort('date', "Date"); ?></th>
				<th><?php echo $this->Paginator->sort('matieres_id', "Matières"); ?></th>
				<th><?php echo $this->Paginator->sort('promotions_id', "Promotions"); ?></th>
				<th class="actions" width="29%"><?php echo __('Actions'); ?></th>
		</tr>
		<?php foreach ($cours as $cour): ?>
		<tr>
			<td><?php echo h($cour['Cour']['author']); ?>&nbsp;</td>
			<td><?php echo h($cour['Cour']['name']); ?>&nbsp;</td>
			<td>
				De <?php echo h($cour['Cour']['heure_debut']); ?> 
				à <?php echo h($cour['Cour']['heure_fin']); ?> 
				le <?php echo h($cour['Cour']['date']); ?>
			</td>
			<td>
				<?php echo $this->Html->link($cour['Matiere']['name'], array('controller' => 'matieres', 'action' => 'view', $cour['Matiere']['id'])); ?>
			</td>
			<td>
				<?php echo $this->Html->link($cour['Promotion']['name'], array('controller' => 'promotions', 'action' => 'view', $cour['Promotion']['id'])); ?>
			</td>
			<td class="actions">
				<?php echo $this->Html->link(__('Détails'), array('action' => 'view', $cour['Cour']['id']), array('class' => 'btn btn-primary')); ?>
				<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $cour['Cour']['id']), array('class' => 'btn btn-primary')); ?>
				<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $cour['Cour']['id']), array('class' => 'btn btn-primary'), __('Êtes vous sûr de vouloir supprimer le cours %s?', $cour['Cour']['name'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
		</table>
		<p>
		<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} sur {:pages}, {:current} entrées sur {:count}, de l\'entrée n°{:start} à {:end}.')
		));
		?>	</p>
		<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('Précédent '), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ' '));
			echo $this->Paginator->next(__('Suivant') . ' >', array(), null, array('class' => 'next disabled'));
		?>
		</div>
	</div>
</div>	
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Ajouter un cours'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Liste des matières'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Liste des promotions'), array('controller' => 'promotions', 'action' => 'index')); ?> </li>
	</ul>
</div>
