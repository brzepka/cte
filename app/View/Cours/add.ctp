    <div class="navigation-bar">
        <ol class="breadcrumb">
            <li>Cours</li>
            <li class="active">Ajout</li>
        </ol>
    </div>

    <div class="mywell">
    <?php 
        echo $this->Form->create('Cour', array("class" => "form-horizontal")); 
        echo $this->Form->hidden('author', array('style'=> 'display: hidden', "value" => "ROBERT DUCHMOL"));
    ?>
    	<fieldset>
    		<legend><?php echo __('Ajouter un Cours'); ?></legend>
        <div class="row">
            <div class="col-lg-6">
                <?php
                    echo $this->Form->input('matieres_id', array('label' => 'Matière', 'class' => 'form-control'));
                    echo $this->Form->input('promotions_id', array('label' => 'Promotion', 'class' => 'form-control'));
                    echo $this->Form->input('name', array('label' => 'Intitulé', 'class' => 'form-control'));
                    echo $this->Form->input('heure_debut', array('label' => 'Début', 'class' => 'form-control', 'type' => "text", "placeholder" => "HH:MM"));
                    echo $this->Form->input('heure_fin', array('label' => 'Fin', 'class' => 'form-control', 'type' => "text", "placeholder" => "HH:MM"));
                    echo $this->Form->input('date', array('label' => 'Date', 'class' => 'form-control', 'type' => "text", "placeholder" => "JJ/MM/AAAA"));
                ?>
            </div>
            <div class="col-lg-6">
                <?php
                    echo $this->Form->input('content', array('label'=>'Contenu','class'=>'form-control', 'style' => 'height: 400px'));
                ?>
            </div>
        </div>
        <div class="row" style="margin-top: 50px">
            <?php echo $this->Form->submit('Valider', array('class' => 'btn btn-primary btn-lg btn-block')); ?>
            <?php echo $this->Form->end(); ?>
        </div>
    	</fieldset>

    </div>