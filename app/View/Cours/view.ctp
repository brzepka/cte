<div class="navigation-bar">
    <ol class="breadcrumb">
        <li>Cours</li>
        <li class="active">Vue</li>
    </ol>
</div>

<div class="mywell">
    <div class="row">
        <div class="col-lg-6">
            <dl>
                <legend><?php echo __('Matiere'); ?></legend>
                <dd>
                    <?php echo $this->Html->link($cour['Matiere']['name'], array('controller' => 'matieres', 'action' => 'view', $cour['Matiere']['id'])); ?>
                    &nbsp;
                </dd>
                <legend><?php echo __('Promotion'); ?></legend>
                <dd>
                    <?php echo $this->Html->link($cour['Promotion']['name'], array('controller' => 'promotions', 'action' => 'view', $cour['Promotion']['id'])); ?>
                    &nbsp;
                </dd>
                <legend><?php echo __('Auteur'); ?></legend>
                <dd>
                    <?php echo h($cour['Cour']['author']); ?>
                    &nbsp;
                </dd>
                <legend><?php echo __('Matiere'); ?></legend>
                <dd>
                    <?php echo h($cour['Cour']['name']); ?>
                    &nbsp;
                </dd>
                <dt><?php echo __('Heure de Debut'); ?></dt>
                <dd>
                    <?php echo h($cour['Cour']['heure_debut']); ?>
                    &nbsp;
                </dd>
                <dt><?php echo __('Heure de Fin'); ?></dt>
                <dd>
                    <?php echo h($cour['Cour']['heure_fin']); ?>
                    &nbsp;
                </dd>
                <dt><?php echo __('Date'); ?></dt>
                <dd>
                    <?php echo h($cour['Cour']['date']); ?>
                    &nbsp;
                </dd>
                <dt><?php echo __('Crée le'); ?></dt>
                <dd>
                    <?php echo h($cour['Cour']['created']); ?>
                    &nbsp;
                </dd>
                <dt><?php echo __('Modifié le'); ?></dt>
                <dd>
                    <?php echo h($cour['Cour']['updated']); ?>
                    &nbsp;
                </dd>
            </dl>
        </div>
        <div class="col-lg-6">
            <dl>
                <dt><?php echo __('Contenu'); ?></dt>
                <dd>
                    <?php echo h($cour['Cour']['content']); ?>
                    &nbsp;
                </dd>
            </dl>
        </div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cour'), array('action' => 'edit', $cour['Cour']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cour'), array('action' => 'delete', $cour['Cour']['id']), null, __('Are you sure you want to delete # %s?', $cour['Cour']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cours'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cour'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matieres'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Promotions'), array('controller' => 'promotions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Promotions'), array('controller' => 'promotions', 'action' => 'add')); ?> </li>
	</ul>
</div>
