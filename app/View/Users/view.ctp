<div class="users view">
	<h2><?php echo __('Utilisateur n°'),h($user['User']['id']); ?></h2>
	<div class="row">
		<div class="col-md-5">
			<table class="table table-bordered">
				<tr>
					<td><?php echo __('Login'); ?></td>
					<td><?php echo h($user['User']['username']); ?></td>
				</tr>
				<tr>
					<td><?php echo __('Mot de passe'); ?></td>
					<td><?php echo h($user['User']['password']); ?></td>
				</tr>
				<tr>
					<td><?php echo __('Rôle'); ?></td>
					<td><?php echo h($user['User']['role']); ?></td>
				</tr>
				<tr>
					<td><?php echo __('e-Mail'); ?></td>
					<td><?php echo h($user['User']['email']); ?></td>
				</tr>
			</table>
		</div>
		<div class="col-md-5">
			<table class="table table-bordered">
				<tr>
					<td><?php echo __('Prénom'); ?></td>
					<td><?php echo h($user['User']['surname']); ?></td>
				</tr>
				<tr>
					<td><?php echo __('Nom'); ?></td>
					<td><?php echo h($user['User']['name']); ?></td>
				</tr>
				<tr>
					<td><?php echo __('Date de création'); ?></td>
					<td><?php echo date_format(new Datetime(h($user['User']['created'])),"H:i:s d/m/Y"); ?></td>
				</tr>
				<tr>
					<td><?php echo __('Date de dernière modification'); ?></td>
					<td><?php echo date_format(new Datetime(h($user['User']['updated'])),"H:i:s d/m/Y"); ?></td>
				</tr>
			</table>
		</div>
	</div>
	<!-- <dl>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($user['User']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Role'); ?></dt>
		<dd>
			<?php echo h($user['User']['role']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Surname'); ?></dt>
		<dd>
			<?php echo h($user['User']['surname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($user['User']['updated']); ?>
			&nbsp;
		</dd>
	</dl> -->
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $user['User']['id']), null, __('Êtes vous sûr de vouloir supprimer l\'utilisateur %s?', $user['User']['username'])); ?> </li>
		<li><?php echo $this->Html->link(__('Liste des utilisateurs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Créer un utilisateur'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('Liste des matières'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Créer une matière'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Matières de l\'utilisateur'); ?></h3>
	<?php if (!empty($user['Matiere'])): ?>
	<table class="table table-bordered" cellpadding="0" cellspacing="0">
	<tr class="info">
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Libellé'); ?></th>
		<th><?php echo __('Contenu'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['Matiere'] as $matiere): ?>
		<tr>
			<td><?php echo $matiere['id']; ?></td>
			<td><?php echo $matiere['name']; ?></td>
			<td><?php echo $matiere['content']; ?></td>
			<td class="actions">
				<div class="btn-group">
					<?php echo $this->Html->link(__('Détails'), array('controller' => 'matieres', 'action' => 'view', $matiere['id']), array('class' => 'btn btn-primary')); ?>
					<?php echo $this->Html->link(__('Modifier'), array('controller' => 'matieres', 'action' => 'edit', $matiere['id']), array('class' => 'btn btn-primary')); ?>
					<?php echo $this->Form->postLink(__('Supprimer'), array('controller' => 'matieres', 'action' => 'delete', $matiere['id']), array('class' => 'btn btn-primary'), __('Êtes vous sûr de vouloir supprimer la matière %s?', $matiere['name'])); ?>
				</div>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('Créer une matière'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
