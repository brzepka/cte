<div class="users form mywell">
<?php echo $this->Form->create('User', array("class" => "form-horizontal")); ?>
	<?php echo $this->Form->input('id');?>
	<fieldset class="row">
		<legend class="col-md-12"><?php echo __('Ajouter un utilisateur'); ?></legend>
		<div class="col-md-6">
			<?php echo $this->Form->input('username', array("label" => "Login", "class" => "form-control col-md-2"));?>
			<?php echo $this->Form->input('password', array("label" => "Mot de passe", "class" => "form-control"));?>
			<?php echo $this->Form->input('role', array("label" => "Rôle", "class" => "form-control"));?>
		</div>
		<div class="col-md-6">
			<?php echo $this->Form->input('surname', array("label" => "Prénom", "class" => "form-control"));?>
			<?php echo $this->Form->input('name', array("label" => "Nom", "class" => "form-control"));?>
			<?php echo $this->Form->input('email', array("label" => "e-Mail", "class" => "form-control"));?>
		</div>
		<div class="col-md-12">
			<?php echo $this->Form->input('Matiere', array("label" => "Matières", "class" => "form-control"));?>
		</div>
	</fieldset>
<?php echo $this->Form->end(array("label" => __('Valider'), "class" => "btn btn-primary")); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Liste des utilisateurs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Liste des matières'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Créer une matière'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
	</ul>
</div>