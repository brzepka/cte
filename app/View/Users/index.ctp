<div class="navigation-bar">
    <ol class="breadcrumb">
        <li class="active">Utilisateurs</li>
    </ol>
</div>

<div class="mywell">
	<div class="users index">
		<table class="table table-bordered" cellpadding="0" cellspacing="0">
		<tr class="info">
				<th><?php echo $this->Paginator->sort('username', "Login"); ?></th>
				<th><?php echo $this->Paginator->sort('role', "Rôle"); ?></th>
				<th><?php echo $this->Paginator->sort('surname', "Prénom"); ?></th>
				<th><?php echo $this->Paginator->sort('name', "Nom"); ?></th>
				<th><?php echo $this->Paginator->sort('email', "e-Mail"); ?></th>
				<th width="28%" class="actions"><?php echo __('Actions'); ?></th>
		</tr>
		<?php foreach ($users as $user):?>
		<tr>
			<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
			<td><?php echo h($user['User']['role']); ?>&nbsp;</td>
			<td><?php echo h($user['User']['surname']); ?>&nbsp;</td>
			<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
			<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
			<td class="actions">
				<div class="btn-group">
					<?php echo $this->Html->link(__('Détails'), array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-primary')); ?>
					<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $user['User']['id']),array('class' => 'btn btn-primary')); ?>
					<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-primary'), __('Êtes vous sûr de vouloir supprimer l\'utilisateur %s?', $user['User']['username'])); ?>
				</div>
			</td>
		</tr>
		<?php endforeach;?>
		</table>
		<p>
		<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} sur {:pages}, {:current} entrées sur {:count}, de l\'entrée n°{:start} à {:end}.')
		));
		?>	</p>
		<div class="pager">
            <?php
				echo $this->Paginator->next(__('Suivant') . ' >', array(), null, array('class' => 'next disabled'));
			?>
        </div>
	</div>
	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
			<li><?php echo $this->Html->link(__('Ajouter un utilisateur'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('Liste des matières'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('Ajouter une matière'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>