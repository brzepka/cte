<div class="matieresUsers form">
<?php echo $this->Form->create('MatieresUser'); ?>
	<fieldset>
		<legend><?php echo __('Edit Matieres User'); ?></legend>
	<?php
		echo $this->Form->input('matiere_id');
		echo $this->Form->input('user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('MatieresUser.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('MatieresUser.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Matieres Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
