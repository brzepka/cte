<div class="matieresUsers view">
<h2><?php echo __('Matieres User'); ?></h2>
	<dl>
		<dt><?php echo __('Matiere'); ?></dt>
		<dd>
			<?php echo $this->Html->link($matieresUser['Matiere']['name'], array('controller' => 'matieres', 'action' => 'view', $matieresUser['Matiere']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($matieresUser['User']['name'], array('controller' => 'users', 'action' => 'view', $matieresUser['User']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Matieres User'), array('action' => 'edit', $matieresUser['MatieresUser']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Matieres User'), array('action' => 'delete', $matieresUser['MatieresUser']['id']), null, __('Are you sure you want to delete # %s?', $matieresUser['MatieresUser']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Matieres Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matieres User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
