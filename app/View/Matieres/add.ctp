<div class="navigation-bar">
    <ol class="breadcrumb">
        <li>Matières</li>
        <li class="active">Ajout</li>
    </ol>
</div>
<div class="matiere form mywell">
<?php echo $this->Form->create('Matiere', array("class" => "form-horizontal")); ?>
	<?php echo $this->Form->input('id');?>
	<fieldset class="row">
		<legend class="col-md-12"><?php echo __('Ajouter une Matière'); ?></legend>
		<div class="col-md-6">
			<?php echo $this->Form->input('name', array("label" => "Nom", "class" => "form-control col-md-2"));?>
			<?php echo $this->Form->input('User', array("label" => "Utilisateur", "class" => "form-control"));?>
		</div>
		<div class="col-md-6">
			<?php echo $this->Form->input('content', array("label" => "Contenu", "class" => "form-control"));?>
		</div>
	</fieldset>
<?php echo $this->Form->end(array("label" => __('Valider'), "class" => "btn btn-primary")); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Liste des matières'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Liste des utilisateurs'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Ajouter un utilisateur'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>