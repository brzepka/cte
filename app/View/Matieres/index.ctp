<div class="navigation-bar">
    <ol class="breadcrumb">
        <li class="active">Matières</li>
    </ol>
</div>
<div class="matieres index">
	<h2><?php echo __('Matières'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<tr>
			<th><?php echo $this->Paginator->sort('id',"Id"); ?></th>
			<th><?php echo $this->Paginator->sort('name', "Nom"); ?></th>
			<th><?php echo $this->Paginator->sort('content', "Contenu"); ?></th>
			<th width="28%" class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($matieres as $matiere): ?>
	<tr>
		<td><?php echo h($matiere['Matiere']['id']); ?>&nbsp;</td>
		<td><?php echo h($matiere['Matiere']['name']); ?>&nbsp;</td>
		<td><?php echo h($matiere['Matiere']['content']); ?>&nbsp;</td>
		<td class="actions">
			<div class="btn-group">
				<?php echo $this->Html->link(__('Détails'), array('action' => 'view', $matiere['Matiere']['id']), array('class' => 'btn btn-primary')); ?>
				<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $matiere['Matiere']['id']),array('class' => 'btn btn-primary')); ?>
				<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $matiere['Matiere']['id']), array('class' => 'btn btn-primary'), __('Êtes vous sûr de vouloir supprimer la matière %s?', $matiere['Matiere']['name'])); ?>
			</div>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} sur {:pages}, {:current} entrées sur {:count}, de l\'entrée n°{:start} à {:end}.')
	));
	?>	</p>
	<div class="pager">
        <?php
			echo $this->Paginator->prev('< ' . __('Précédent '), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ' '));
			echo $this->Paginator->next(__('Suivant') . ' >', array(), null, array('class' => 'next disabled'));
		?>
    </div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Ajouter une matière'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Liste des utilisateurs'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Ajouter un utilisateur'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
