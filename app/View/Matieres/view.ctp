<div class="navigation-bar">
    <ol class="breadcrumb">
        <li>Matières</li>
        <li class="active">Vue</li>
    </ol>
</div>
<div class="matieres view">
	<h2><?php echo __('Matiere n°'), h($matiere['Matiere']['id']); ?></h2>
	<div class="row">
		<div class="col-md-3">
			<table class="table table-bordered">
				<tr>
					<td><?php echo __('Nom'); ?></td>
					<td><?php echo h($matiere['Matiere']['name']); ?></td>
				</tr>
			</table>
			<dl>
				<dt><?php echo __('Contenu'); ?></dt>
				<dd>
					<?php echo h($matiere['Matiere']['content']); ?>
					&nbsp;
				</dd>
			</dl>
		</div>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Modifier la matière'), array('action' => 'edit', $matiere['Matiere']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Supprimer la matière'), array('action' => 'delete', $matiere['Matiere']['id']), null, __('Êtes vous sûr de vouloir supprimer la matière %s?', $matiere['Matiere']['name'])); ?> </li>
		<li><?php echo $this->Html->link(__('Liste des matières'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Ajouter une matière'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('Liste des utilisateurs'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Ajouter un utilisateur'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Utilisateurs'); ?></h3>
	<?php if (!empty($matiere['User'])): ?>
	<table class="table table-bordered" cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Role'); ?></th>
		<th><?php echo __('Surname'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th width="28%" class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($matiere['User'] as $user): ?>
		<tr>
			<td><?php echo $user['username']; ?></td>
			<td><?php echo $user['role']; ?></td>
			<td><?php echo $user['surname']; ?></td>
			<td><?php echo $user['name']; ?></td>
			<td><?php echo $user['email']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id']), array('class' => 'btn btn-primary')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id']), array('class' => 'btn btn-primary')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), array('class' => 'btn btn-primary'), __('Êtes vous sûr de vouloir supprimer l\'utilisateur %s?', $user['username'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('Ajouter un utilisateur'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
